/*
*	Tesis "Drones autonomos"
*	Alumno: Gonzalo Romero
*	Profesor: Ramiro Donoso
*	Este Codigo es el principal aqui esta creada la vista de la app, ademas aqui se crean los subscribes
*	que se comunican con el servidor mqtt que a su vez se comunican con el dron para obtener la informacion
*	de navegacion e imagen.
*/
package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import org.autonomous4j.control.Brain;
import org.autonomous4j.control.Demo;
import org.autonomous4j.control.JoysticControl;
import org.autonomous4j.control.subscribe.SubscriberNav;
import org.autonomous4j.control.subscribe.SubscriberNavCapture;
import org.autonomous4j.control.subscribe.SubscriberVideo;
import org.autonomous4j.tracking.A4jBlackBox.Movement;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Controller;
import org.lwjgl.input.Controllers;

import utils.Imagen;
import utils.PanelImagen;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.List;

import javax.swing.JLabel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.Font;
import java.awt.Label;
import java.awt.Canvas;
import java.awt.Panel;
import javax.swing.ImageIcon;
/**
 * 
 * @author Gonzalo
 * @version 1.0
 *
 */
@SuppressWarnings("unused")
public class Principal extends JFrame {//key listener fuera

	private static final long serialVersionUID = 1L;
	
	private KeyPanel contentPane;
	private PanelImagen panelImagen;
	private static Brain b;
	//private List<NavData> listData; 
	private static boolean isButtonTri;
	
	
	private SubscriberVideo clientImage;
	private SubscriberNav clientRoll;
	private SubscriberNav clientPitch;
	private SubscriberNav clientYaw;
	private SubscriberNav clientX;
	private SubscriberNav clientY;
	private SubscriberNav clientZ;
	private SubscriberNav clientBattery;
	private SubscriberNav clientAltitude;

	private JLabel labelRoll;
	private JLabel labelPitch;
	private JLabel labelYaw;
	private JLabel labelX;
	private JLabel labelY;
	private JLabel labelZ;
	private JLabel labelBatery;
	private JLabel labelAltitude;
	private JLabel label;
	private Button btnFrontalHd;
	private Button btnBotcamerap;
	private Button btnReset;
	private Button btnTakeOff;
	private Button btnLand;


	private static float axisNavX;
	private static float axisNavY;
	private static float axisNavZ;
	private static float axisNavPitch;
	private static float axisNavRoll;
	private static float axisNavYaw;
	private static float axisNavBattery;
	private static float axisNavAltitude;
	private static float axisNav;
	private static float [] dataEstadisticaY = new float[10000];
	
	public static void main(String[] args) {
		b = new Brain();
		Principal frame = new Principal();
		frame.setVisible(true);
		frame.connect();
		performDemo(b);
	}

	public Principal() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1268, 544);
		contentPane = new KeyPanel(b);
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panelImagen = new PanelImagen();
		panelImagen.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelImagen.setSize(640, 360);
		panelImagen.setLocation(10, 10);
		contentPane.add(panelImagen);
		
		labelRoll = new JLabel("0");
		labelRoll.setFont(new Font("DialogInput", Font.BOLD, 23));
		labelRoll.setBounds(690, 80, 128, 42);
		contentPane.add(labelRoll);		

		labelPitch = new JLabel("0");
		labelPitch.setFont(new Font("DialogInput", Font.BOLD, 23));
		labelPitch.setBounds(836, 80, 131, 42);
		contentPane.add(labelPitch);

		labelYaw = new JLabel("0");
		labelYaw.setFont(new Font("DialogInput", Font.BOLD, 23));
		labelYaw.setBounds(982, 80, 140, 42);
		contentPane.add(labelYaw);

		labelX = new JLabel("0");
		labelX.setFont(new Font("DialogInput", Font.BOLD, 23));
		labelX.setBounds(690, 197, 111, 42);
		contentPane.add(labelX);

		labelY = new JLabel("0");
		labelY.setFont(new Font("DialogInput", Font.BOLD, 23));
		labelY.setBounds(798, 197, 110, 42);
		contentPane.add(labelY);

		labelZ = new JLabel("0");
		labelZ.setFont(new Font("DialogInput", Font.BOLD, 23));
		labelZ.setBounds(923, 197, 111, 42);
		contentPane.add(labelZ);

		labelBatery = new JLabel("0");
		labelBatery.setFont(new Font("DialogInput", Font.BOLD, 27));
		labelBatery.setBounds(1160, -4, 86, 42);
		contentPane.add(labelBatery);

		labelAltitude = new JLabel("0");
		labelAltitude.setFont(new Font("DialogInput", Font.BOLD, 27));
		labelAltitude.setBounds(1190, 116, 56, 49);
		contentPane.add(labelAltitude);

		JLabel lblAxis = new JLabel("Axis:");
		lblAxis.setFont(new Font("DialogInput", Font.BOLD, 35));
		lblAxis.setBounds(701, 0, 140, 29);
		contentPane.add(lblAxis);

		JLabel lblYaw = new JLabel("Yaw");
		lblYaw.setFont(new Font("DialogInput", Font.BOLD, 30));
		lblYaw.setBounds(987, 40, 115, 32);
		contentPane.add(lblYaw);

		JLabel lblRoll = new JLabel("Roll");
		lblRoll.setFont(new Font("DialogInput", Font.BOLD, 30));
		lblRoll.setBounds(690, 45, 111, 32);
		contentPane.add(lblRoll);

		JLabel lblPitch = new JLabel("Pitch");
		lblPitch.setFont(new Font("DialogInput", Font.BOLD, 30));
		lblPitch.setBounds(836, 40, 115, 32);
		contentPane.add(lblPitch);

		JLabel lblSpeed = new JLabel("Speed:");
		lblSpeed.setFont(new Font("DialogInput", Font.BOLD, 30));
		lblSpeed.setBounds(705, 118, 121, 42);
		contentPane.add(lblSpeed);

		JLabel lblX = new JLabel("x");
		lblX.setFont(new Font("DialogInput", Font.BOLD, 30));
		lblX.setBounds(705, 155, 111, 42);
		contentPane.add(lblX);

		JLabel lblY = new JLabel("y");
		lblY.setFont(new Font("DialogInput", Font.BOLD, 30));
		lblY.setBounds(820, 160, 111, 32);
		contentPane.add(lblY);

		JLabel lblZ = new JLabel("z");
		lblZ.setFont(new Font("DialogInput", Font.BOLD, 30));
		lblZ.setBounds(937, 155, 111, 42);
		contentPane.add(lblZ);

		JLabel lblBattery = new JLabel("Battery:");
		lblBattery.setFont(new Font("DialogInput", Font.BOLD, 30));
		lblBattery.setBounds(1011, -5, 151, 42);
		contentPane.add(lblBattery);

		JLabel lblAltitude = new JLabel("Altitude:");
		lblAltitude.setFont(new Font("DialogInput", Font.BOLD, 30));
		lblAltitude.setBounds(1001, 118, 174, 42);
		contentPane.add(lblAltitude);
		
		label = new JLabel("%");
		label.setFont(new Font("DialogInput", Font.BOLD, 27));
		label.setBounds(1209, 0, 37, 35);
		contentPane.add(label);
		
		Label label_1 = new Label("Camara:");
		label_1.setFont(new Font("Consolas", Font.BOLD, 17));
		label_1.setBackground(Color.LIGHT_GRAY);
		label_1.setBounds(146, 382, 82, 27);
		contentPane.add(label_1);
		
	
		//============================================================================================================================================================
		//CAMARA FRONTAL HD
		//============================================================================================================================================================

		btnFrontalHd = new Button("Frontal HD",b);
		btnFrontalHd.setFont(new Font("Consolas", Font.BOLD, 18));
		btnFrontalHd.setBackground(new Color(255, 127, 80));
		btnFrontalHd.setForeground(new Color(0, 0, 0));
		btnFrontalHd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//b.viewFrontalCamera();
			}
		});
		btnFrontalHd.setBounds(15, 415, 166, 29);
		contentPane.add(btnFrontalHd);
		//============================================================================================================================================================
		//CAMARA BOT 480P		
		//============================================================================================================================================================
		btnBotcamerap = new Button("BotCamera 480p",b);
		btnBotcamerap.setFont(new Font("Consolas", Font.BOLD, 18));
		btnBotcamerap.setForeground(new Color(0, 0, 0));
		btnBotcamerap.setBackground(new Color(255, 127, 80));
		btnBotcamerap.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//b.viewBackCamera();
			}
		});
		btnBotcamerap.setBounds(196, 415, 174, 29);
		contentPane.add(btnBotcamerap);
		//============================================================================================================================================================		
		// RESET
		//============================================================================================================================================================
		btnReset = new Button("Reset",b);
		btnReset.setFont(new Font("Consolas", Font.BOLD, 25));
		btnReset.setForeground(Color.BLACK);
		btnReset.setBackground(Color.WHITE);
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				b.reset();
			}
		});
		btnReset.setBounds(1095, 71, 115, 29);
		contentPane.add(btnReset);
		//============================================================================================================================================================
		//TAKE OFF
		//============================================================================================================================================================
		btnTakeOff = new Button("Take Off",b);
		btnTakeOff.setForeground(new Color(0, 100, 0));
		btnTakeOff.setBackground(Color.GREEN);
		btnTakeOff.setFont(new Font("Consolas", Font.BOLD, 20));
		btnTakeOff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				b.takeoff().hold(3000);
				
			}
		});
		btnTakeOff.setBounds(413, 378, 126, 42);
		contentPane.add(btnTakeOff);
		//============================================================================================================================================================
		//LAND
		//============================================================================================================================================================
		btnLand = new Button("Land",b);
		btnLand.setFont(new Font("Consolas", Font.BOLD, 20));
		btnLand.setForeground(new Color(139, 69, 19));
		btnLand.setBackground(new Color(255, 165, 0));
		btnLand.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				b.land();
			}
		});
		btnLand.setBounds(413, 436, 126, 42);
		contentPane.add(btnLand);
		//============================================================================================================================================================
		//CLIENTES
		//============================================================================================================================================================
		inicioCliente();

	}
	/**
	 * @author Gonzalo
	 */

	/*Funcion encargado de crear la subscripcion con el servidor*/		
	public void inicioCliente() {
		clientImage = new SubscriberVideo(panelImagen,"image");
		clientAltitude= new SubscriberNav(labelAltitude,axisNavAltitude, "altitude");
		clientBattery = new SubscriberNav(labelBatery,axisNavBattery,"battery");

		clientX = new SubscriberNav(labelX,axisNavX,"speedx");
		clientY = new SubscriberNav(labelY,axisNav,"speedy");
		clientZ = new SubscriberNav(labelZ,axisNavZ,"speedz");

		clientRoll = new SubscriberNav(labelRoll,axisNavRoll,"roll");
		clientPitch = new SubscriberNav(labelPitch,axisNavPitch,"pitch");
		clientYaw = new SubscriberNav(labelYaw,axisNavYaw,"yaw");	
		
	}
/*Inicia la subscripcion para el recibo de datos*/	
	public void connect() {
		clientImage.start();
		clientRoll.start();
		clientPitch.start();
		clientYaw.start();
		clientX.start();
		clientY.start();
		clientZ.start();
		clientBattery.start(); 
		clientAltitude.start();
	}

/* Aqui monta las configuraciones de conexion monta un "cerebro" que es la cual comunica al dron 
 * Ademas queda a la esperade cualquier evento del teclado para ejecutarlo como movimiento, esto
 * gracias al keypanel
 */
	private static void performDemo(Brain br) {
		if (br.connect()) {
			try {
				commands(br);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				br.disconnect();
			}
		} else {
			System.out.println("No Drone Connection.");
		}
	}
	
	private static void commands(Brain br) {
		int i =0;
		boolean f=true;
		while(true) {
			if(i<10000 && axisNav!=0) {
				System.out.println(axisNav);
				dataEstadisticaY [i]=axisNav;
				i++;
			}
			if(i==10000 && f) {
				System.out.println("Promedio: "+promData(dataEstadisticaY));
				System.out.println("Desviacion Estandar: "+devStandarData(dataEstadisticaY));
				System.out.println("valor menor registrado: "+menorData(dataEstadisticaY));
				System.out.println("valor mayor registrado: "+mayorData(dataEstadisticaY));
				f=false;
			}
		}
	}

	private static float devStandarData(float[] dataEstadisticaY2) {
		float prom = promData(dataEstadisticaY2);
		float suma = 0;
		for(int i=0; i<10000;i++) {
			suma=(float) (suma+Math.pow(dataEstadisticaY2[i]-prom,2));
		}
		suma = suma/10000;
		return (float) Math.sqrt(suma);
	}

	private static float promData(float[] dataEstadisticaY2) {
		float suma = 0;
		for(int i=0; i<10000;i++) {
			suma=suma+dataEstadisticaY2[i];
		}
		return (suma/10000);
	}
	
	private static float mayorData(float[] dataEstadisticaY){
		  float numeromayor = dataEstadisticaY[0];

			        for(int i=0; i<dataEstadisticaY.length; i++){
			            if(dataEstadisticaY[i]>numeromayor){ 
			                numeromayor = dataEstadisticaY[i];
			              }
			        }
		
		return numeromayor;
	}
	private static float menorData(float[] dataEstadisticaY){
		  float numeromenor = dataEstadisticaY[0];
			        for(int i=0; i<dataEstadisticaY.length; i++){
			            if(dataEstadisticaY[i]<numeromenor){ 
			                numeromenor = dataEstadisticaY[i];
			              }
			        }
		
		return numeromenor;
	}
}

