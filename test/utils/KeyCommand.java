package utils;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import org.autonomous4j.control.Brain;

public class KeyCommand implements KeyListener{
	private Brain b;
	boolean i =true;
	

	public KeyCommand(Brain b) {
		this.b=b;
	}
	
	public void keyPressed(KeyEvent e) {
		System.out.println(e.getKeyCode());
		if(e.getKeyCode()== KeyEvent.VK_RIGHT && i) {
			System.out.println("derecha");
			i=false;
			b.goRight(300);
		}
		else if(e.getKeyCode()== KeyEvent.VK_LEFT && i) {
			System.out.println("izquierda");
			i=false;
			b.goLeft(300);
		}
		else if(e.getKeyCode()== KeyEvent.VK_UP && i) {
			System.out.println("forward");
			i=false;
			b.forward(300);
		}
		else if(e.getKeyCode()== KeyEvent.VK_DOWN && i) {
			System.out.println("backward");
			i=false;
			b.backward(300);
		}
		//====================================================================================================================================		
		//down (tecla s)
		//====================================================================================================================================
		else  if(e.getKeyCode()== 83 && i) {
			System.out.println("down");
			i=false;
			b.down(400);	
		}
		//====================================================================================================================================		
		//up (tecla w)
		//====================================================================================================================================
		else if(e.getKeyCode()== 87 && i) {
			System.out.println("up");
			i=false;
			b.up(400);
		}
		//====================================================================================================================================		
		//takeoff (tecla t)
		//====================================================================================================================================
		else if(e.getKeyCode()==84 && i) {
			System.out.println("take off!!");
			i=false;
			b.takeoff().hold(3000);
			b.stay().hold(1000);
		}
		//====================================================================================================================================
		//land (tecla l)
		//====================================================================================================================================
		else if(e.getKeyCode()==76 && i) {
			System.out.println("land");
			i=false;
			b.land();
		}
		//====================================================================================================================================
		//cambio camara (tecla c)
		//====================================================================================================================================
		else if(e.getKeyCode()==67 && i) {
			System.out.println("cambio camara");
			i=false;
			b.changeCamera();
		}
		else if(e.getKeyCode()==65 && i) {
			System.out.println("giro derecha!!");
			i=false;
			b.move(0, 0, 0, 1f);
		}
		else if(e.getKeyCode()==68 && i) {
			System.out.println("giro izquierda!!");
			i=false;
			b.move(0, 0, 0, -1f);
		}
		//===========================================================================================================================
		//detener(tecla q)
		//===========================================================================================================================
		else if(e.getKeyCode()==81 && i) {
			System.out.println("stop");
			i=false;
			b.stay();
		}	
	}
	
	public void keyReleased(KeyEvent e) {
		b.stay();
		i=true;
	}

	public void keyTyped(KeyEvent e) {
		System.out.println("keyTyped");
	}
}
