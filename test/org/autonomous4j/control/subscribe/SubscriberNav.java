package org.autonomous4j.control.subscribe;

import javax.swing.JLabel;

import org.eclipse.paho.client.mqttv3.*;

import utils.PanelImagen;

public class SubscriberNav extends Thread{

     public static final String BROKER_URL = "tcp://localhost:1883";
     private MqttClient client;
 	 private JLabel labelNav; 	 
 	 private String propiedad;
	 private float axisNav;
    
     public SubscriberNav(JLabel labelNav,float axisNav,String propiedad) {
   	  this.propiedad=propiedad;
   	  this.labelNav = labelNav;
   	  this.axisNav=axisNav;
   	  String clientId = "demonio"+propiedad;
         try {
             client = new MqttClient(BROKER_URL, clientId);
         }
         catch (MqttException e) {
             e.printStackTrace();
             System.exit(1);
         }
    }

     public void run() {

          try {   
        	  SubscribeNavCallback susC =  new SubscribeNavCallback(labelNav,axisNav);
              client.setCallback(susC);              
              client.connect();
              client.subscribe("a4jnavdata/"+propiedad);
              
          }
          catch (MqttException e) {
              e.printStackTrace();
              System.exit(1);
          }

     }

}
