package org.autonomous4j.control.subscribe;

import org.eclipse.paho.client.mqttv3.*;

import utils.Imagen;
import utils.PanelImagen;
import utils.Serializar;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.lang.Throwable;
import java.util.Objects;

import javax.swing.JLabel;

public class SubscribeVideoCallback implements MqttCallback {
	
	private PanelImagen panelImagen;
	
	public SubscribeVideoCallback(PanelImagen panelImagen) {
		this.panelImagen = panelImagen;
	}

     @Override
     public void connectionLost(Throwable cause) {}

     @Override
     public void messageArrived(String topic, MqttMessage message) {
    	 //if(Objects.equals(topic,"a4jvideodata/image")) {
    	 byte[] buff =message.getPayload();    	 
    	 
    	// Create new image and get reference to backing data
    	   BufferedImage image = new BufferedImage(640, 360, BufferedImage.TYPE_3BYTE_BGR);
    	   final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
    	   System.arraycopy(buff, 0, targetPixels, 0, buff.length);
    	   //image = Imagen.scale(image, 200, 200);
    	 panelImagen.setImage(image);
    	 //}
    	// if(Objects.equals(topic, "a4jnavdatalistener/roll")) {
    		 
    	// }
     }

     @Override
     public void deliveryComplete(IMqttDeliveryToken token) {}

}
