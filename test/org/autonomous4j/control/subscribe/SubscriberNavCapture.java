/*
*pruebas con listas de datos
*pendientes
*/
package org.autonomous4j.control.subscribe;

import java.util.List;

import javax.swing.JLabel;

import org.eclipse.paho.client.mqttv3.*;

import gui.NavData;
import utils.PanelImagen;

public class SubscriberNavCapture extends Thread{

     public static final String BROKER_URL = "tcp://localhost:1883";
     private MqttClient client;
 	 private List<NavData> listData;  	 
 	 private String propiedad;
     public SubscriberNavCapture(List<NavData> listData,String propiedad) {
    	  this.propiedad=propiedad;
    	  this.listData = listData;
    	  String clientId = "demonio"+propiedad;
          try {
              client = new MqttClient(BROKER_URL, clientId);
          }
          catch (MqttException e) {
              e.printStackTrace();
              System.exit(1);
          }
     }

     public void run() {

          try {        	 
              client.setCallback(new SubscribeNavCallbackCapture(listData));              
              client.connect();
              client.subscribe("a4jnavdata/"+propiedad);              
          }
          catch (MqttException e) {
              e.printStackTrace();
              System.exit(1);
          }

     }

}
