/*
*pruebas con listas de datos
*pendientes
*/
package org.autonomous4j.control.subscribe;

import org.eclipse.paho.client.mqttv3.*;

import gui.NavData;
import utils.Imagen;
import utils.PanelImagen;
import utils.Serializar;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.lang.Throwable;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Objects;

import javax.swing.JLabel;
//import com.mathworks.engine.*;

public class SubscribeNavCallbackCapture implements MqttCallback {
	
	
	private List<NavData> listData;
	private int i=0;
	private long finTime=0;
	private long iniTime=0;
	
	
	public SubscribeNavCallbackCapture(List<NavData> listData) {
		this.listData = listData;
	}

     @Override
     public void connectionLost(Throwable cause) {}

     @Override
     public void messageArrived(String topic, MqttMessage message) {
    	 finTime = System.currentTimeMillis();
    	 NavData navData= new NavData(message.toString(),i,finTime);
    	 listData.add(navData);
    	 i++;
    	 }

     @Override
     public void deliveryComplete(IMqttDeliveryToken token) {}

	

}
