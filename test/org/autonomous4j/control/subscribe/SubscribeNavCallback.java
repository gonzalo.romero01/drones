package org.autonomous4j.control.subscribe;

import org.eclipse.paho.client.mqttv3.*;

import utils.Imagen;
import utils.PanelImagen;
import utils.Serializar;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.lang.Throwable;
import java.nio.ByteBuffer;
import java.util.Objects;

import javax.swing.JLabel;

public class SubscribeNavCallback implements MqttCallback {
	
	
	private JLabel labelNav;
	private float axisnav;
	private static float [] data= new float[10000];
	private int i =0;

	
	public SubscribeNavCallback(JLabel labelNav,float axisnav) {
	
		this.labelNav = labelNav;
		this.axisnav=axisnav;
	}
	

     @Override
     public void connectionLost(Throwable cause) {}

     @Override
     public void messageArrived(String topic, MqttMessage message) {
    	 if(topic.equals("a4jnavdata/roll") && (i!=10)) {
    		 System.out.println(message.toString());
    		 data[i]=Float.parseFloat(message.toString());
    		 i++;
    	 }
    	 labelNav.setText(message.toString());
    	 }

     @Override
     public void deliveryComplete(IMqttDeliveryToken token) {}

}
