package org.autonomous4j.control;


public class Demo {
    
    public static void main(String[] args) {
    	Brain b = new Brain();
    	performDemo(b);
    	
    	/*
    	 * Instalar Servidor Mosquitto (Servidor MQTT)
    	 * 		sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa
    	 * 		sudo apt-get update	
    	 * 		sudo apt install mosquitto
    	 * 
    	 * Instalar cliente Mosquitto 
    	 * 		sudo apt install mosquitto-clients
    	 * 
    	 * 
    	 * Ejecutar en una terminal un cliente mosquito para ver los movimientos que realiza el drone
    	 * 		mosquitto_sub -h 127.0.0.1 -p 1883 -d -t a4jflight/#
    	 * 		mosquitto_sub -h 127.0.0.1 -p 1883 -d -t a4jflight/movement
    	 * 		mosquitto_sub -h 127.0.0.1 -p 1883 -d -t a4jnavdata/#
    	 * 		mosquitto_sub -h 127.0.0.1 -p 1883 -d -t a4jnavdata/battery
    	 * 
    	 */
    }

    
    private static void performDemo(Brain br) {
        //Attempt to connect to the drone
        if (br.connect()) {
            try {
                //Execute drone commands
                commands(br);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                br.disconnect();
            }
        } else {
            System.out.println("No Drone Connection.");
        }
    }

    private static void commands(Brain br) {
    	while(true);
        //br.takeoff().hold(3000);
        //br.stay().hold(3000);
        //br.land();
    }

}
